package marcosgribel.br.com.desafioandroidb2w.configuration;

import android.app.Application;
import android.content.Context;

import marcosgribel.br.com.desafioandroidb2w.configuration.di.component.AppComponent;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.component.DaggerAppComponent;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.AppModule;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class App extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
