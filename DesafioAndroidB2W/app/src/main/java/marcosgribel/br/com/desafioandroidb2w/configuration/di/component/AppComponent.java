package marcosgribel.br.com.desafioandroidb2w.configuration.di.component;

import javax.inject.Singleton;

import dagger.Component;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.AppModule;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.DepartmentModule;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.HomeModule;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.ProductModule;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Singleton
@Component(
        modules = {
                AppModule.class
        }
)
public interface AppComponent {

        HomeComponent plus(HomeModule module);

        ProductComponent plus(ProductModule module);

        DepartmentComponent plus(DepartmentModule module);

}
