package marcosgribel.br.com.desafioandroidb2w.configuration.di.component;

import dagger.Subcomponent;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.DepartmentModule;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.ProductModule;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.scope.AppScope;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.ui.activity.DepartmentListActivity;
import marcosgribel.br.com.desafioandroidb2w.ui.activity.SubDepartmentListActivity;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@AppScope
@Subcomponent(
        modules = {
                DepartmentModule.class
        }
)
public interface DepartmentComponent {

    void inject(DepartmentListActivity activity);

    void inject(SubDepartmentListActivity activity);

}
