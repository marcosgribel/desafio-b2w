package marcosgribel.br.com.desafioandroidb2w.configuration.di.component;

import dagger.Subcomponent;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.HomeModule;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.scope.AppScope;
import marcosgribel.br.com.desafioandroidb2w.ui.activity.HomeActivity;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@AppScope
@Subcomponent(
        modules = {
                HomeModule.class
        }
)
public interface HomeComponent {

    void inject(HomeActivity activity);

}
