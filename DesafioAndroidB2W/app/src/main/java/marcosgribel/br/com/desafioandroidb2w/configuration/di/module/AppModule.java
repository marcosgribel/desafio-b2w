package marcosgribel.br.com.desafioandroidb2w.configuration.di.module;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import marcosgribel.br.com.desafioandroidb2w.configuration.App;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Module
public class AppModule {

    App app;

    public AppModule(App app) {
        this.app = app;
    }


    @Provides
    @Singleton
    public App provideApplication() {
        return app;
    }




}
