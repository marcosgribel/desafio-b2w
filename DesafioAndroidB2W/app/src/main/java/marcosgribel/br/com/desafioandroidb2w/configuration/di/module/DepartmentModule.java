package marcosgribel.br.com.desafioandroidb2w.configuration.di.module;

import dagger.Module;
import dagger.Provides;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.scope.AppScope;
import marcosgribel.br.com.desafioandroidb2w.interactor.DepartmentInteractor;
import marcosgribel.br.com.desafioandroidb2w.interactor.impl.DepartmentInteractorImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.DepartmentListActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl.DepartmentListActivityPresenterImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.DepartmentView;
import marcosgribel.br.com.desafioandroidb2w.service.DepartmentService;
import marcosgribel.br.com.desafioandroidb2w.service.DepartmentServiceImpl;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Module
public class DepartmentModule {


    DepartmentView view;

    public DepartmentModule(DepartmentView view) {
        this.view = view;
    }

    @AppScope
    @Provides
    DepartmentView provideDepartmentView(){
        return this.view;
    }


    @AppScope
    @Provides
    DepartmentListActivityPresenter provideDepartmentListActivityPresenter(DepartmentListActivityPresenterImpl presenter){
        return presenter;
    }


    @AppScope
    @Provides
    DepartmentInteractor provideDepartmentInteractor(DepartmentInteractorImpl interactor){
        return interactor;
    }

    @AppScope
    @Provides
    DepartmentService provideDepartmentService(DepartmentServiceImpl service){
        return service;
    }


}
