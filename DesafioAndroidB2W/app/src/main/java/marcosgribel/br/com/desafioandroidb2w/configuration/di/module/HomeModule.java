package marcosgribel.br.com.desafioandroidb2w.configuration.di.module;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

import dagger.Module;
import dagger.Provides;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.scope.AppScope;
import marcosgribel.br.com.desafioandroidb2w.interactor.HomeInteractor;
import marcosgribel.br.com.desafioandroidb2w.interactor.impl.HomeInteractorImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.HomeActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl.HomeActivityPresenterImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.HomeView;
import marcosgribel.br.com.desafioandroidb2w.service.HomeService;
import marcosgribel.br.com.desafioandroidb2w.service.impl.HomeServiceImpl;

@Module
public class HomeModule {

    HomeView view;

    public HomeModule(HomeView view) {
        this.view = view;
    }

    @AppScope
    @Provides
    HomeView provideHomeView(){
        return this.view;
    }

    @AppScope
    @Provides
    HomeActivityPresenter provideHomeActivityPresenter(HomeActivityPresenterImpl presenter){
        return presenter;
    }

    @AppScope
    @Provides
    HomeInteractor provideHomeInteractor(HomeInteractorImpl interactor){
        return interactor;
    }

    @AppScope
    @Provides
    HomeService provideHomeService(HomeServiceImpl service){
        return service;
    }

}
