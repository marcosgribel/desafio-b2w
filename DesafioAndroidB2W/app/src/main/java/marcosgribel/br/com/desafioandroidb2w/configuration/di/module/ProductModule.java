package marcosgribel.br.com.desafioandroidb2w.configuration.di.module;

import dagger.Module;
import dagger.Provides;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.scope.AppScope;
import marcosgribel.br.com.desafioandroidb2w.interactor.ProductInteractor;
import marcosgribel.br.com.desafioandroidb2w.interactor.impl.ProductInteractorImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductDetailActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductGridActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductListFragmentPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl.ProductDetailActivityPresenterImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl.ProductGridActivityPresenterImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl.ProductListFragmentPresenterImpl;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.HomeView;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.ProductView;
import marcosgribel.br.com.desafioandroidb2w.service.ProductService;
import marcosgribel.br.com.desafioandroidb2w.service.impl.ProductServiceImpl;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Module
public class ProductModule {

    ProductView view;


    public ProductModule(ProductView view) {
        this.view = view;
    }

    @AppScope
    @Provides
    ProductView provideHomeView(){
        return this.view;
    }


    @AppScope
    @Provides
    ProductListFragmentPresenter provideProductListFragmentPresenter(ProductListFragmentPresenterImpl presenter){
        return presenter;
    }

    @AppScope
    @Provides
    ProductDetailActivityPresenter provideProductDetailActivityPresenter(ProductDetailActivityPresenterImpl presenter){
        return presenter;
    }

    @AppScope
    @Provides
    ProductGridActivityPresenter provideProductGridActivityPresenter(ProductGridActivityPresenterImpl presenter){
        return presenter;
    }

    @AppScope
    @Provides
    ProductInteractor provideProductInteractor(ProductInteractorImpl interactor){
        return interactor;
    }

    @AppScope
    @Provides
    ProductService provideProductService(ProductServiceImpl service){
        return service;
    }



}
