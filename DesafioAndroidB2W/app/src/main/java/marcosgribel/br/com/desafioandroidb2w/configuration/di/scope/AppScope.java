package marcosgribel.br.com.desafioandroidb2w.configuration.di.scope;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Copyright 2016, Bradesco Seguros - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 * <p>
 * Developed by BRQ IT Services
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface AppScope {
}
