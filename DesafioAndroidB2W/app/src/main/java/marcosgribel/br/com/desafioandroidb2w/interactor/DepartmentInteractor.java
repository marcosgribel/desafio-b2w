package marcosgribel.br.com.desafioandroidb2w.interactor;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import rx.Observable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface DepartmentInteractor {

    Observable<DepartmentList> getDepartmentById(String departmentId);

    Observable<ProductList> getDepartmentGalleryById(String departmentId);

    Observable<DataItemList> getDepartmentDataItem(String menuId);

}
