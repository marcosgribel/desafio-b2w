package marcosgribel.br.com.desafioandroidb2w.interactor.impl;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.DepartmentInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.service.DepartmentService;
import rx.Observable;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class DepartmentInteractorImpl implements DepartmentInteractor {

    @Inject
    DepartmentService service;



    @Inject
    public DepartmentInteractorImpl() {
    }

    @Override
    public Observable<DepartmentList> getDepartmentById(String departmentId) {
        return service.getDepartmentById(departmentId);
    }

    @Override
    public Observable<ProductList> getDepartmentGalleryById(String departmentId) {
        return service.getDepartmentGalleryById(departmentId);
    }

    @Override
    public Observable<DataItemList> getDepartmentDataItem(String menuId) {
        return service.getDepartmentDataItem(menuId);
    }
}
