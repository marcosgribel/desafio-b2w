package marcosgribel.br.com.desafioandroidb2w.interactor.impl;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.HomeInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Home;
import marcosgribel.br.com.desafioandroidb2w.service.HomeService;
import rx.Observable;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class HomeInteractorImpl implements HomeInteractor {

    @Inject
    HomeService service;

    @Inject
    public HomeInteractorImpl() {
    }

    @Override
    public Observable<Home> home() {
        return service.home();
    }

}
