package marcosgribel.br.com.desafioandroidb2w.interactor.impl;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.ProductInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.service.ProductService;
import rx.Observable;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class ProductInteractorImpl implements ProductInteractor {

    @Inject
    ProductService service;

    @Inject
    public ProductInteractorImpl() {

    }

    @Override
    public Observable<DataItemList> getProducts() {
        return service.getProducts();
    }

    @Override
    public Observable<ProductList> getProductsById(String id) {
        return service.getProductsById(id, null);
    }

    @Override
    public Observable<ProductList> getByDepartmentFiltered(String menuId, int offset, int limit) {
        return service.getByDepartmentFiltered(menuId, offset, limit);
    }
}
