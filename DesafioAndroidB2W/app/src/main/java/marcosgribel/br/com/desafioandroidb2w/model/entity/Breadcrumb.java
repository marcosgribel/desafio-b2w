package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name = "breadcrumb")
public class Breadcrumb implements Serializable {

    @Element(name = "id")
    private String id;
    @Element(name = "link")
    private String link;
    @Element(name = "label")
    private String label;
    @Element(name = "slugLabel")
    private String slugLabel;

    public Breadcrumb() {
    }

    public Breadcrumb(String id, String link, String label, String slugLabel) {
        this.id = id;
        this.link = link;
        this.label = label;
        this.slugLabel = slugLabel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSlugLabel() {
        return slugLabel;
    }

    public void setSlugLabel(String slugLabel) {
        this.slugLabel = slugLabel;
    }
}
