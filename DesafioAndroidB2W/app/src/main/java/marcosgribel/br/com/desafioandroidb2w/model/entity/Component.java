
package marcosgribel.br.com.desafioandroidb2w.model.entity;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Component {

    @SerializedName("children")
    @Expose
    private List<Child> children = new ArrayList<>();
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("type")
    @Expose
    private String type;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Component() {
    }

    /**
     * 
     * @param title
     * @param children
     * @param type
     * @param key
     */
    public Component(List<Child> children, String title, String key, String type) {
        super();
        this.children = children;
        this.title = title;
        this.key = key;
        this.type = type;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
