
package marcosgribel.br.com.desafioandroidb2w.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Content implements Serializable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("component")
    @Expose
    private Component component;
    @SerializedName("segmentation_match")
    @Expose
    private String segmentationMatch;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Content() {
    }

    /**
     * 
     * @param position
     * @param id
     * @param platform
     * @param segmentationMatch
     * @param component
     * @param page
     * @param name
     */
    public Content(String id, String name, String page, String platform, String position, Component component, String segmentationMatch) {
        super();
        this.id = id;
        this.name = name;
        this.page = page;
        this.platform = platform;
        this.position = position;
        this.component = component;
        this.segmentationMatch = segmentationMatch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

    public String getSegmentationMatch() {
        return segmentationMatch;
    }

    public void setSegmentationMatch(String segmentationMatch) {
        this.segmentationMatch = segmentationMatch;
    }

}
