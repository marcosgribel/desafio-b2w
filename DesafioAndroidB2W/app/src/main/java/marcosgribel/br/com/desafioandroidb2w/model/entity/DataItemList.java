package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name="data", strict=false)
public class DataItemList {

    @ElementList(inline=true)
    protected List<ItemList> itemListList;

    public List<ItemList> getItemListList()
    {
        return this.itemListList;
    }

}
