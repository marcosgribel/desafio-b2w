package marcosgribel.br.com.desafioandroidb2w.model.entity;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

import org.apache.commons.lang3.StringEscapeUtils;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Root(name="parent", strict=false)
public class Department {
    @ElementList(inline=true)
    ArrayList<MenuItem> menuItemList;
    @Attribute(required=false)
    private String name;

    public MenuItem getDepartmentById(String paramString)
    {
        Iterator localIterator = this.menuItemList.iterator();
        while (localIterator.hasNext())
        {
            MenuItem localMenuItem = (MenuItem)localIterator.next();
            if (localMenuItem.getMenuId().equals(paramString)) {
                return localMenuItem;
            }
        }
        return null;
    }

    public List<FilterableItem> getFilterList()
    {
        ArrayList localArrayList = new ArrayList();
        localArrayList.addAll(getMenuItemList());
        return localArrayList;
    }

    public String getFilterTitle()
    {
        return StringEscapeUtils.unescapeHtml3(this.name);
    }

    public String getGroupFilter()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        Iterator localIterator = this.menuItemList.iterator();
        while (localIterator.hasNext())
        {
            MenuItem localMenuItem = (MenuItem)localIterator.next();
            if (localMenuItem.isSelected().booleanValue()) {
                localStringBuilder.append(localMenuItem.getFilter()).append(" ");
            }
        }
        return localStringBuilder.toString();
    }

    public ArrayList<MenuItem> getMenuItemList()
    {
        return this.menuItemList;
    }

    public String getParentName()
    {
        return StringEscapeUtils.unescapeHtml3(this.name);
    }
}