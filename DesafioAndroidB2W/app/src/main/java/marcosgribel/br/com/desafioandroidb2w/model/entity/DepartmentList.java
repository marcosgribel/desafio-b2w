package marcosgribel.br.com.desafioandroidb2w.model.entity;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Root(name="data", strict=false)
public class DepartmentList
{
    @ElementList(inline=true)
    private List<Department> departmentList;
    private List<Object> flatItemsList = new ArrayList();
    private List<Integer> headerItemsIdxList = new ArrayList();

    public List<Department> getDepartmentList()
    {
        return this.departmentList;
    }

    public List<Object> getFlatItemsList()
    {
        return this.flatItemsList;
    }

    public List<Integer> getParentsIndex()
    {
        return this.headerItemsIdxList;
    }

    public void init()
    {
        Iterator localIterator = this.departmentList.iterator();
        while (localIterator.hasNext())
        {
            Department localDepartment = (Department)localIterator.next();
            this.headerItemsIdxList.add(Integer.valueOf(this.flatItemsList.size()));
            this.flatItemsList.add(localDepartment.getParentName());
            this.flatItemsList.addAll(localDepartment.getMenuItemList());
        }
    }
}
