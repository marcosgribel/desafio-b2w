package marcosgribel.br.com.desafioandroidb2w.model.entity;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

public abstract class FilterItem implements FilterableItem {
    private Boolean isSelected = Boolean.valueOf(false);

    public abstract String getFilter();

    public abstract String getFilterTitle();

    public Boolean isSelected()
    {
        return this.isSelected;
    }

    public void setSelected(Boolean paramBoolean)
    {
        this.isSelected = paramBoolean;
    }
}
