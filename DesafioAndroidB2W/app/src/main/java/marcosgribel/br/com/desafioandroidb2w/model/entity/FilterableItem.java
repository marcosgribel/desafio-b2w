package marcosgribel.br.com.desafioandroidb2w.model.entity;

import java.io.Serializable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public abstract interface FilterableItem extends Serializable {
    public abstract String getFilter();

    public abstract String getFilterTitle();

    public abstract Boolean isSelected();

    public abstract void setSelected(Boolean paramBoolean);
}