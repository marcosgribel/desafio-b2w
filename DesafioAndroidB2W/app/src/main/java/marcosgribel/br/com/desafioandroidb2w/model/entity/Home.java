
package marcosgribel.br.com.desafioandroidb2w.model.entity;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Home {

    @SerializedName("content")
    @Expose
    private Content content;
    @SerializedName("contentmiddle1")
    @Expose
    private Content contentmiddle1;
    @SerializedName("x01")
    @Expose
    private Content x01;
    @SerializedName("x02")
    @Expose
    private Content x02;
    @SerializedName("x03")
    @Expose
    private Content x03;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Home() {
    }

    /**
     *
     * @param x02
     * @param x03
     * @param contentmiddle1
     * @param content
     */
    public Home(Content content, Content contentmiddle1, Content x02, Content x03) {
        super();
        this.content = content;
        this.contentmiddle1 = contentmiddle1;
        this.x02 = x02;
        this.x03 = x03;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Content getContentmiddle1() {
        return contentmiddle1;
    }

    public void setContentmiddle1(Content contentmiddle1) {
        this.contentmiddle1 = contentmiddle1;
    }


    public Content getX01() {
        return x01;
    }

    public void setX01(Content x01) {
        this.x01 = x01;
    }

    public Content getX02() {
        return x02;
    }

    public void setX02(Content x02) {
        this.x02 = x02;
    }

    public Content getX03() {
        return x03;
    }

    public void setX03(Content x03) {
        this.x03 = x03;
    }
}
