package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name = "images")
public class Images implements Serializable {

    @Element(name = "thumbimage")
    private ThumbImage[] thumbs;


    public Images() {
    }

    public Images(ThumbImage[] thumbs) {
        this.thumbs = thumbs;
    }

    public ThumbImage[] getThumbs() {
        return thumbs;
    }

    public void setThumbs(ThumbImage[] thumbs) {
        this.thumbs = thumbs;
    }
}
