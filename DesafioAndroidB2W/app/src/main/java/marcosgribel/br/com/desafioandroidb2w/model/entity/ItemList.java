package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.apache.commons.lang3.StringEscapeUtils;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name="list", strict=false)
public class ItemList implements Serializable {

    @ElementList(inline=true, required=false)
    ArrayList<Product> productList;
    @Attribute(required=false)
    String title;

    public String getProdIds()
    {
        StringBuilder localStringBuilder = new StringBuilder();
        Iterator localIterator = this.productList.iterator();
        while (localIterator.hasNext()) {
            localStringBuilder.append(((Product)localIterator.next()).getProdId()).append(" ");
        }
        return localStringBuilder.toString().trim();
    }

    public ArrayList<Product> getProductList()
    {
        return this.productList;
    }

    public String getTitle()
    {
        return StringEscapeUtils.unescapeHtml3(this.title);
    }

    public boolean hasProductList()
    {
        return (this.productList != null) && (!this.productList.isEmpty());
    }

    public void setProductList(ArrayList<Product> paramArrayList)
    {
        this.productList = paramArrayList;
    }

    public void setTitle(String paramString)
    {
        this.title = paramString;
    }

}
