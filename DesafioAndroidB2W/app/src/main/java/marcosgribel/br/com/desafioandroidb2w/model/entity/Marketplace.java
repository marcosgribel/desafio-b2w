package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(strict = false)
public class Marketplace implements Serializable {

    @Attribute(required=false)
    private Boolean hasPartnersWithStock;
    @Attribute(required=false)
    private Boolean isExclusiveMarketPlace;
    @ElementList(inline=true, required=false)
    private List<Partner> partnerList;
    @Attribute(required=false)
    private String smallestPriceOfAllPartners;
    private Money smallestPriceOfAllPartnersMoney;

    public Boolean getHasPartnersWithStock()
    {
        return this.hasPartnersWithStock;
    }

    public List<Partner> getPartnerList()
    {
        return this.partnerList;
    }

//    public Money getSmallestPriceOfAllPartners()
//    {
//        if (this.smallestPriceOfAllPartnersMoney == null) {
//            this.smallestPriceOfAllPartnersMoney = new Money(this.smallestPriceOfAllPartners);
//        }
//        return this.smallestPriceOfAllPartnersMoney;
//    }

    public Boolean isExclusiveMarketPlace()
    {
        return this.isExclusiveMarketPlace;
    }
}
