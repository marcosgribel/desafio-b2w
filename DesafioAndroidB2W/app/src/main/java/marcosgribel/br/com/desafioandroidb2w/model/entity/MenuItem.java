package marcosgribel.br.com.desafioandroidb2w.model.entity;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

import android.os.*;
import android.os.Parcel;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name="menuItem", strict=false)
public class MenuItem extends FilterItem implements Comparable<MenuItem>, Parcelable {
    @Attribute
    private String group;
    @Attribute(required=false)
    private boolean haveChildren = true;
    @Attribute
    private String menuId;
    @Attribute
    private String name;
    @Attribute(required=false)
    private String tag;
    private String unescapedName = null;


    public MenuItem() {
    }

    protected MenuItem(Parcel in) {
        group = in.readString();
        haveChildren = in.readByte() != 0;
        menuId = in.readString();
        name = in.readString();
        tag = in.readString();
        unescapedName = in.readString();
    }

    public static final Creator<MenuItem> CREATOR = new Creator<MenuItem>() {
        @Override
        public MenuItem createFromParcel(Parcel in) {
            return new MenuItem(in);
        }

        @Override
        public MenuItem[] newArray(int size) {
            return new MenuItem[size];
        }
    };

    public int compareTo(MenuItem paramMenuItem)
    {
        if (this.name.contains("CoolStuff")) {
            return -1;
        }
        if (paramMenuItem.getName().contains("CoolStuff")) {
            return 1;
        }
        return this.name.compareTo(paramMenuItem.getName());
    }

    public String getFilter()
    {
        return getFormattedGroup();
    }

    public String getFilterTitle()
    {
        return getName();
    }

    public String getFormattedGroup()
    {
        if ("".equals(this.group)) {
            return this.tag;
        }
        return this.group.replace(", ", " ");
    }

    public String getGroup()
    {
        return this.group;
    }

    public String getMenuId()
    {
        return this.menuId;
    }

    public String getName()
    {
        if (StringUtils.isBlank(this.unescapedName))
        {
            this.unescapedName = StringEscapeUtils.unescapeHtml3(this.name);
            if (StringUtils.isNotBlank(this.unescapedName)) {
                this.unescapedName = this.unescapedName.replace("&apos;", "'");
            }
        }
        return this.unescapedName;
    }

    public String getTag()
    {
        return this.tag;
    }

    public boolean haveChildren()
    {
        return this.haveChildren;
    }

    public void setHaveChildren(boolean paramBoolean)
    {
        this.haveChildren = paramBoolean;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(group);
        dest.writeByte((byte) (haveChildren ? 1 : 0));
        dest.writeString(menuId);
        dest.writeString(name);
        dest.writeString(tag);
        dest.writeString(unescapedName);
    }
}

