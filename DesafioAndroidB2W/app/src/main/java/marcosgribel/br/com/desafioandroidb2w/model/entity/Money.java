package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Pattern;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class Money implements Serializable {

    private static final NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
    private static final NumberFormat numberFormatter = NumberFormat.getNumberInstance(new Locale("pt", "BR"));
    private BigDecimal amount = BigDecimal.ZERO;


    public Money() {}

    public Money(String paramString) {
        try {
            if (isCurrencyString(paramString).booleanValue())
            {
                this.amount = new BigDecimal(numberFormatter.parse(paramString.replace("R$ ", "")).doubleValue());
                this.amount = this.amount.setScale(2, RoundingMode.HALF_UP);
            }
            return;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String formatDecimal(float paramFloat)
    {
        return currencyFormatter.format(paramFloat).replace("R$", "R$ ");
    }

    public static String formatDecimal(String paramString)
    {
        if (StringUtils.isBlank(paramString)) {
            return "R$ 0,00";
        }
        return formatDecimal(Float.parseFloat(paramString));
    }

    private Boolean isCurrencyString(String paramString)
    {
        return Boolean.valueOf(Pattern.compile("(R\\$\\s)?(\\d{1,3}\\.)?(\\d{3}\\.)*?\\d{1,3},\\d*").matcher(paramString).matches());
    }

    private void setAmout(BigDecimal paramBigDecimal)
    {
        this.amount = paramBigDecimal.setScale(2, RoundingMode.HALF_UP);
    }

//    public Money add(Money paramMoney)
//    {
//        paramMoney = this.amount.add(paramMoney.amount);
//        Money localMoney = new Money();
//        localMoney.setAmout(paramMoney);
//        return localMoney;
//    }

    public boolean biggerThan(Money paramMoney)
    {
        return getAmount().compareTo(paramMoney.getAmount()) == 1;
    }

    public boolean equals(Money paramMoney)
    {
        return getAmount().compareTo(paramMoney.getAmount()) == 0;
    }

    public BigDecimal getAmount()
    {
        return this.amount;
    }

    public Boolean hasValue()
    {
        if (this.amount.doubleValue() != 0.0D) {}
        for (boolean bool = true;; bool = false) {
            return Boolean.valueOf(bool);
        }
    }

    public int hashCode()
    {
        if (this.amount == null) {}
        for (int i = 0;; i = this.amount.hashCode()) {
            return i + 31;
        }
    }

    public boolean lessThan(Money paramMoney)
    {
        return getAmount().compareTo(paramMoney.getAmount()) == -1;
    }

    public String prettyPrint()
    {
        return currencyFormatter.format(this.amount.doubleValue()).replace("R$", "R$ ");
    }

//    public Money substract(Money paramMoney)
//    {
//        paramMoney = this.amount.subtract(paramMoney.amount);
//        Money localMoney = new Money();
//        localMoney.setAmout(paramMoney);
//        return localMoney;
//    }
}
