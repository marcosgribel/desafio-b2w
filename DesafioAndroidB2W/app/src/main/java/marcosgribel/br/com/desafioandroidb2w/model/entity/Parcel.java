package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name = "parcel")
public class Parcel implements Serializable {

    @Attribute(required = false)
    private String total;
    @Attribute(required = false)
    private String value;
    @Attribute(required = false)
    private String interestRate;

    public Parcel() {
    }

    public Parcel(String total, String value, String interestRate) {
        this.total = total;
        this.value = value;
        this.interestRate = interestRate;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }
}

