package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.apache.commons.lang3.StringEscapeUtils;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name = "partner")
public class Partner implements Serializable {

    @Attribute(required=false)
    private Boolean hasPickupStore;
    @Attribute(required=false)
    private String id;
//    @ElementList(inline=true, required=false)
//    private List<PartnerInstallment> partnerInstallmentList;
    @Attribute(required=false)
    private String partnerName;
    @Attribute(required=false)
    private String salesPrice;
    private Money salesPriceMoney;
    private String unescapedPartnerName;

    public String getId()
    {
        return this.id;
    }

//    public List<PartnerInstallment> getPartnerInstallmentList()
//    {
//        return this.partnerInstallmentList;
//    }

    public String getPartnerName()
    {
        if (this.unescapedPartnerName == null) {
            this.unescapedPartnerName = StringEscapeUtils.unescapeHtml3(this.partnerName);
        }
        return this.unescapedPartnerName;
    }

    public Money getSalesPrice()
    {
        if (this.salesPriceMoney == null) {
            this.salesPriceMoney = new Money(this.salesPrice);
        }
        return this.salesPriceMoney;
    }

    public Boolean hasPickupStore()
    {
        return this.hasPickupStore;
    }

}
