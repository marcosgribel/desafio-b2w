package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name = "paymentOption")
public class PaymentOption implements Serializable {

    @Attribute(required = false)
    private String yearlyInterest;
    @ElementList(name = "parcel")
    private List<Parcel> parcels;
    @Attribute(required = false)
    private String monthlyInterest;
    @Attribute(required = false)
    private String name;

    public PaymentOption() {
    }

    public PaymentOption(String yearlyInterest, List<Parcel> parcels, String monthlyInterest, String name) {
        this.yearlyInterest = yearlyInterest;
        this.parcels = parcels;
        this.monthlyInterest = monthlyInterest;
        this.name = name;
    }

    public String getYearlyInterest() {
        return yearlyInterest;
    }

    public void setYearlyInterest(String yearlyInterest) {
        this.yearlyInterest = yearlyInterest;
    }

    public List<Parcel> getParcels() {
        return parcels;
    }

    public void setParcels(List<Parcel> parcels) {
        this.parcels = parcels;
    }

    public String getMonthlyInterest() {
        return monthlyInterest;
    }

    public void setMonthlyInterest(String monthlyInterest) {
        this.monthlyInterest = monthlyInterest;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
