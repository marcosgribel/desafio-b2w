package marcosgribel.br.com.desafioandroidb2w.model.entity;

import android.os.*;
import android.os.Parcel;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(strict = false)
public class Product implements Serializable, Parcelable {

    public static final String SKU_DEFAULT = "DEFAULT";
    @Attribute(required=false)
    protected String billetDiscountPercent;
    @Attribute(required=false)
    protected String billetPrice;
    private Money billetPriceMoney;
    @Element(name="crossSell", required=false)
//    protected CrossSell crossSell;
    @Attribute(required=false)
    protected String description;
    @Attribute(required=false)
    protected String image;
    @Attribute(required=false)
    protected String installment;
    @Attribute(required=false)
    protected Boolean isWhiteLine;
//    private FashionSkuList mFashionSkuList = new FashionSkuList();
    @Element(name="marketplace", required=false)
    protected Marketplace marketPlaceInfo;
    @Attribute(required=false)
    protected String mqImage;
    @Attribute(required=false)
    protected String name;
    @Attribute(required=false)
    protected String numReviews;
    @Attribute(required=false)
    protected String price;
    private Money priceDiscountMoney;
    private String priceDiscountPercent;
    @Attribute(required=false)
    protected String priceFrom;
    private Money priceFromMoney;
    private Money priceMoney;
    @Attribute(required=false)
    protected String prodId;
    @Attribute(required=false)
    protected String rating;
//    @ElementList(inline=true, required=false)
//    @Path("skuInfo")
//    protected List<SkuDiff> skuDiffList;
    @ElementList(inline=true, required=false)
    @Path("skus")
    protected List<Sku> skuList;
//    @ElementList(inline=true, required=false)
//    protected List<SpecTecs> specTecsList;
    @Attribute(required=false)
    protected Boolean stock;
    @ElementList(inline=true, required=false)
    @Path("images")
    protected List<ThumbImage> thumbImageList;
    private String unescapedDescription = null;
    private String unescapedName = null;
    @Attribute(required=false)
    protected String url;
    @Attribute(required=false)
    protected String video;

    public Product() {
    }


    protected Product(Parcel in) {
        billetDiscountPercent = in.readString();
        billetPrice = in.readString();
        description = in.readString();
        image = in.readString();
        installment = in.readString();
        mqImage = in.readString();
        name = in.readString();
        numReviews = in.readString();
        price = in.readString();
        priceDiscountPercent = in.readString();
        priceFrom = in.readString();
        prodId = in.readString();
        rating = in.readString();
        unescapedDescription = in.readString();
        unescapedName = in.readString();
        url = in.readString();
        video = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getProdId() {
        return prodId;
    }


    public String getBilletDiscountPercent()
    {
        return this.billetDiscountPercent;
    }

    public Money getBilletPrice()
    {
        if (this.billetPriceMoney == null) {
            this.billetPriceMoney = new Money(this.billetPrice);
        }
        return this.billetPriceMoney;
    }


    public String getDescription()
    {
        if (this.unescapedDescription == null) {
            this.unescapedDescription = StringEscapeUtils.unescapeHtml3(this.description);
        }
        return this.unescapedDescription;
    }



    public String getFirstPartnerId()
    {
        return ((Partner)this.marketPlaceInfo.getPartnerList().get(0)).getId();
    }

    public String getHqImage()
    {
        List localList = getImageList();
        if ((localList != null) && (localList.size() > 0)) {
            return ((ThumbImage)localList.get(0)).getUrl();
        }
        return this.mqImage;
    }

    public String getImage()
    {
        return this.image;
    }

    public List<ThumbImage> getImageList()
    {
        if (this.mqImage != null) {}
        for (String str = this.mqImage;; str = this.image)
        {
            if ((this.thumbImageList == null) || (this.thumbImageList.isEmpty()))
            {
                this.thumbImageList = new ArrayList();
                this.thumbImageList.add(new ThumbImage(str));
            }
            return this.thumbImageList;
        }
    }


    public String getInstallment()
    {
        return this.installment;
    }

    public Marketplace getMarketPlaceInfo()
    {
        return this.marketPlaceInfo;
    }

    public String getMqImage()
    {
        return this.mqImage;
    }

    public String getName()
    {
        if (this.unescapedName == null) {
            this.unescapedName = StringEscapeUtils.unescapeHtml3(this.name).replace("&apos;", "'");
        }
        return this.unescapedName;
    }

    public Integer getNumReviews()
    {
        try
        {
            Integer localInteger = Integer.valueOf(this.numReviews);
            return localInteger;
        }
        catch (Exception localException) {}
        return Integer.valueOf(0);
    }

//    public Money getPrice()
//    {
//        if (this.priceMoney == null) {
//            this.priceMoney = new Money(this.price);
//        }
//        return this.priceMoney;
//    }


    public String getPrice() {
        return price;
    }

    public Money getPriceFrom()
    {
        if (this.priceFromMoney == null) {
            this.priceFromMoney = new Money(this.priceFrom);
        }
        return this.priceFromMoney;
    }



    public Double getRating()
    {
        try
        {
            double d = new BigDecimal(this.rating).setScale(1, 4).doubleValue();
            return Double.valueOf(d);
        }
        catch (Exception localException) {}
        return Double.valueOf(-1.0D);
    }



    public String getShareText()
    {
        return String.format("%s - %s", new Object[] { getName(), getUrl() });
    }

    public String getSinglePartnerName()
    {
        return ((Partner)this.marketPlaceInfo.getPartnerList().get(0)).getPartnerName();
    }



    public Boolean getStock()
    {
        return this.stock;
    }

    public String getUrl()
    {
        return this.url;
    }

    public String getVideo()
    {
        if ((hasVideo().booleanValue()) && (!this.video.startsWith("http://"))) {
            this.video = ("http://" + this.video);
        }
        return this.video;
    }

    public String getVideoIdentifier()
    {
        if (hasVideo().booleanValue())
        {
            String str2 = this.video;
            String str1 = str2;
            if (this.video.contains("?list")) {
                str1 = str2.substring(0, str2.lastIndexOf("?list"));
            }
            if (this.video.contains("?rel")) {
                return str1.substring(str1.lastIndexOf("/") + 1, str1.indexOf("?rel"));
            }
            return str1.substring(str1.lastIndexOf("/") + 1);
        }
        return null;
    }

    public Boolean hasBilletDiscount()
    {
        boolean bool = false;
        if (this.billetDiscountPercent == null) {
            return Boolean.valueOf(false);
        }
        if (!this.billetDiscountPercent.equals("0%")) {
            bool = true;
        }
        return Boolean.valueOf(bool);
    }

    public Boolean hasBilletPrice()
    {
        return getBilletPrice().hasValue();
    }

//    public Boolean hasCrossSell()
//    {
//        if (this.crossSell != null) {}
//        for (boolean bool = true;; bool = false) {
//            return Boolean.valueOf(bool);
//        }
//    }



    public Boolean hasPriceFrom()
    {
        return getPriceFrom().hasValue();
    }

//    public Boolean hasTechnicalInfo()
//    {
//        if (getSpecTecsList() == null) {
//            return Boolean.valueOf(false);
//        }
//        if (getSpecTecsList().size() < 2) {
//            return Boolean.valueOf(false);
//        }
//        if (((SpecTecs)getSpecTecsList().getProducts(1)).getSpecTecList() == null) {
//            return Boolean.valueOf(false);
//        }
//        return Boolean.valueOf(true);
//    }

    public Boolean hasVideo()
    {
        if ((!StringUtils.isBlank(this.video)) && (this.video.contains("youtube"))) {}
        for (boolean bool = true;; bool = false) {
            return Boolean.valueOf(bool);
        }
    }

    public boolean isExclusiveMarketPlace()
    {
        return (isMarketplaceProduct()) && (this.marketPlaceInfo.isExclusiveMarketPlace().booleanValue());
    }



    public boolean isMarketplaceProduct()
    {
        return (this.marketPlaceInfo != null) && (!this.marketPlaceInfo.getPartnerList().isEmpty());
    }

    public boolean isMarketplaceSinglePartner()
    {
        return (isMarketplaceProduct()) && (this.marketPlaceInfo.getPartnerList().size() == 1);
    }

    public Boolean isWhiteLine()
    {
        return this.isWhiteLine;
    }

    public void setRating(Double paramDouble)
    {
        this.rating = paramDouble.toString();
    }

    public boolean showPartnerList()
    {
        if (!isMarketplaceProduct()) {}
        while ((isExclusiveMarketPlace()) && (isMarketplaceSinglePartner())) {
            return false;
        }
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(billetDiscountPercent);
        dest.writeString(billetPrice);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(installment);
        dest.writeString(mqImage);
        dest.writeString(name);
        dest.writeString(numReviews);
        dest.writeString(price);
        dest.writeString(priceDiscountPercent);
        dest.writeString(priceFrom);
        dest.writeString(prodId);
        dest.writeString(rating);
        dest.writeString(unescapedDescription);
        dest.writeString(unescapedName);
        dest.writeString(url);
        dest.writeString(video);
    }

}
