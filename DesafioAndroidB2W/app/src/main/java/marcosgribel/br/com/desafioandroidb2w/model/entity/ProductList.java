package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

@Root(name="data", strict=false)
public class ProductList
        implements Serializable
{
    @ElementList(inline=true)
    protected List<Product> productList;

    public List<Product> getProductList()
    {
        return this.productList;
    }

    public Boolean hasProduct()
    {
        if (!this.productList.isEmpty()) {}
        for (boolean bool = true;; bool = false) {
            return Boolean.valueOf(bool);
        }
    }

    public void setProductList(List<Product> paramList)
    {
        this.productList = paramList;
    }

    public Product singleResult()
    {
        return (Product)this.productList.get(0);
    }
}
