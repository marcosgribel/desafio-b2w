package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.apache.commons.lang3.StringEscapeUtils;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

@Root(name="sku", strict=false)
public class Sku
        implements Serializable {
    @Attribute(required = false)
    private String image;
    @Attribute(required = false)
    private String name;
    private String unescapedName = null;
    private String unescapedValue = null;
    @Attribute(required = false)
    private String value;

    public String getImage() {
        return this.image;
    }

    public String getName() {
        if (this.unescapedName == null) {
            this.unescapedName = StringEscapeUtils.unescapeHtml3(this.name);
        }
        return this.unescapedName;
    }

    public String getValue() {
        if (this.unescapedValue == null) {
            this.unescapedValue = StringEscapeUtils.unescapeHtml3(this.value);
        }
        return this.unescapedValue;
    }

    public String toString() {
        return this.name;
    }
}