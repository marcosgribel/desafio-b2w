package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name = "specTec")
public class SpecTec {

    @Element(name = "value")
    private String value;
    @Element(name = "key")
    private String key;


    public SpecTec() {
    }

    public SpecTec(String value, String key) {
        this.value = value;
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
