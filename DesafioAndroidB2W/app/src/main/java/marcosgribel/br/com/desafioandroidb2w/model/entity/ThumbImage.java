package marcosgribel.br.com.desafioandroidb2w.model.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
@Root(name="thumbimage", strict=false)
public class ThumbImage implements Serializable {

    @Attribute
    private String url;

    public ThumbImage() {}

    public ThumbImage(String paramString)
    {
        this.url = paramString;
    }

    public String getUrl()
    {
        return this.url;
    }

    public String toString()
    {
        return this.url;
    }
}
