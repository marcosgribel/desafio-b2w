package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Child;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Content;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.ViewHolder> {

    private Context context;
    private List<Child> children = new ArrayList<>();


    public BannerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_banner, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Child child = children.get(position);

        Glide.with(context)
                .load(child.getImage())
                .fitCenter()
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return children.size();
    }


    public void addAll(List<Child> itens){
        children.addAll(itens);
        notifyDataSetChanged();
    }


    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_banner)
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }
    }
}
