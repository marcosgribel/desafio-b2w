package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Department;
import marcosgribel.br.com.desafioandroidb2w.model.entity.MenuItem;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class DepartmentAdapter extends BaseAdapter {

    private Context context;
    private List<MenuItem> itens = new ArrayList<>();
    private Drawable defaultIcon;

    public DepartmentAdapter(Context context) {
        this.context = context;
        this.defaultIcon = context.getResources().getDrawable(R.drawable.ic_new_department);
    }

    public DepartmentAdapter(Context context, List<MenuItem> itens) {
        this(context);
        this.itens = itens;

    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public MenuItem getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public void addAll(List<MenuItem> itens){
        this.itens.addAll(itens);
        notifyDataSetChanged();
    }

    public List<MenuItem> getItens() {
        return itens;
    }

    @BindView(R.id.txt_department_name)
    TextView txtName;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MenuItem item = getItem(position);

        if( convertView == null ){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_department, null);
        }

        ButterKnife.bind(this, convertView);

        String strIconName = "ic_" + item.getMenuId();
        int icon = this.context.getResources().getIdentifier(strIconName, "drawable", this.context.getPackageName());

        if(icon == 0){
            txtName.setCompoundDrawablesWithIntrinsicBounds(this.defaultIcon, null, null, null);
        }else {
            txtName.setCompoundDrawablesWithIntrinsicBounds(this.context.getResources().getDrawable(icon), null, null, null);
        }

        txtName.setText(item.getName());

        return convertView;

    }




}
