package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class GridProductAdapter extends BaseAdapter {

    Context context;
    List<Product> itens = new ArrayList<>();

    public GridProductAdapter(Context context) {
        this.context = context;
    }

    public GridProductAdapter(Context context, List<Product> itens) {
        this(context);
        this.itens = itens;
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public Product getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public void addAll(List<Product> itens){
        this.itens.addAll(itens);
        notifyDataSetChanged();
    }

    public List<Product> getItens() {
        return itens;
    }

    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.img_product)
    ImageView photo;
    @BindView(R.id.txt_product_name)
    TextView txtName;
    @BindView(R.id.txt_product_price)
    TextView txtPrice;
    @BindView(R.id.txt_product_installments)
    TextView txtInstallments;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Product product = getItem(position);

        if(convertView == null ){
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_product_medium, parent, false);
        }
        ButterKnife.bind(this, convertView);

        Glide.with(context)
                .load(product.getImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(photo);


        txtName.setText(product.getName());
        txtPrice.setText(product.getPrice());
        txtInstallments.setText(product.getInstallment().replace(" sem juros", ""));


        return convertView;
    }
}
