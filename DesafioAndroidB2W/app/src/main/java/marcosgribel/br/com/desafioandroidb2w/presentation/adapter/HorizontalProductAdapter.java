package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;
import marcosgribel.br.com.desafioandroidb2w.ui.activity.ProductDetailActivity;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class HorizontalProductAdapter extends RecyclerView.Adapter<HorizontalProductAdapter.HorizontalProductViewHolder> {

    Context context;
    List<Product> itens = new ArrayList<>();



    public HorizontalProductAdapter(Context context, List<Product> itens) {
        this.context = context;
        this.itens = itens;
    }

    @Override
    public HorizontalProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_product_small, parent, false);
        return new HorizontalProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HorizontalProductViewHolder holder, int position) {

        final Product product = itens.get(position);

        Glide.with(context)
                .load(product.getImage())
                .into(holder.photo);

        holder.txtName.setText(product.getName());
        holder.txtPrice.setText(product.getPrice());
        holder.txtInstallments.setText(product.getInstallment().replace(" sem juros", ""));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itens.size();
    }


    public void addAll(List<Product> products){
        this.itens.addAll(products);
        notifyDataSetChanged();
    }

    public void onItemClick(Product product){
        context.startActivity(ProductDetailActivity.newActivity(context, product));
    }


    public class HorizontalProductViewHolder extends RecyclerView.ViewHolder {


        View itemView;
        @BindView(R.id.img_product)
        ImageView photo;
        @BindView(R.id.txt_product_name)
        TextView txtName;
        @BindView(R.id.txt_product_price)
        TextView txtPrice;
        @BindView(R.id.txt_product_installments)
        TextView txtInstallments;

        public HorizontalProductViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            this.itemView = itemView;
        }


    }
}
