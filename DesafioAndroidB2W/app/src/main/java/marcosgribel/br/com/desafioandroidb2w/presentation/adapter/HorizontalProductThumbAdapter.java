package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ThumbImage;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class HorizontalProductThumbAdapter extends RecyclerView.Adapter<HorizontalProductThumbAdapter.HorizontalProductThumbAdapterViewHolder>   {

    Context context;
    List<ThumbImage> itens = new ArrayList<>();


    public HorizontalProductThumbAdapter() {
    }

    public HorizontalProductThumbAdapter(Context context, List<ThumbImage> itens) {
        this.context = context;
        this.itens = itens;
    }

    @Override
    public HorizontalProductThumbAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_thumbimage_product, parent, false);

        return new HorizontalProductThumbAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HorizontalProductThumbAdapterViewHolder holder, int position) {
        ThumbImage thumb = getItem(position);

        Glide.with(context)
                .load(thumb.getUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        holder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .override(400, 400)
                .fitCenter()
                .into(holder.imageView);
    }


    @Override
    public int getItemCount() {
        return itens.size();
    }


    public ThumbImage getItem(int position){
        return itens.get(position);
    }


    public class HorizontalProductThumbAdapterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progress_thumb)
        ProgressBar progressBar;
        @BindView(R.id.img_thumb)
        ImageView imageView;

        public HorizontalProductThumbAdapterViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }
}
