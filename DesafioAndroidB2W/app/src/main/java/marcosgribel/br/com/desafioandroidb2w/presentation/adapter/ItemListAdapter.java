package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ItemList;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class ItemListAdapter extends BaseAdapter {

    Context context;
    List<ItemList> itens = new ArrayList<>();

    public ItemListAdapter(Context context) {
        this.context = context;
    }


    @Override
    public int getCount() {
        return itens.size();
    }

    public void addAll(List<ItemList> itens){
        this.itens.addAll(itens);
        notifyDataSetChanged();
    }


    @Override
    public ItemList getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @BindView(R.id.txt_title_product)
    TextView txtTitle;
    @BindView(R.id.product_recycler_view)
    RecyclerView recyclerView;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null ){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_product_list, null);
        }

        ButterKnife.bind(this, convertView);

        ItemList item = getItem(position);

        txtTitle.setText(item.getTitle());

        HorizontalProductAdapter adapter = new HorizontalProductAdapter(context, item.getProductList());

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setAdapter(adapter);

        return convertView;
    }




}
