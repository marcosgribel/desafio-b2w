package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class NavMenuAdapater extends BaseAdapter {

    Context context;
    TypedArray mIcons;
    String[] mTitles;

    public NavMenuAdapater(Context context, TypedArray mIcons, String[] mTitles) {
        this.context = context;
        this.mIcons = mIcons;
        this.mTitles = mTitles;
    }

    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        TextView textView = (TextView) convertView;
        textView.setText(this.mTitles[position]);
        textView.setCompoundDrawablesWithIntrinsicBounds(this.mIcons.getDrawable(position), null, null, null);

        return convertView;
    }
}
