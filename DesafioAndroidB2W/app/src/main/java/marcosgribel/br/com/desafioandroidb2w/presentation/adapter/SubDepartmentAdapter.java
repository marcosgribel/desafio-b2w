package marcosgribel.br.com.desafioandroidb2w.presentation.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.model.entity.MenuItem;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class SubDepartmentAdapter extends BaseAdapter {

    private Context context;
    private List<MenuItem> itens = new ArrayList<>();

    public SubDepartmentAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return itens.size();
    }

    @Override
    public MenuItem getItem(int position) {
        return itens.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public void addAll(List<MenuItem> itens){
        this.itens.addAll(itens);
        notifyDataSetChanged();
    }


    @BindView(R.id.txt_subdepartment_name)
    TextView txtName;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if( convertView == null ){
            convertView = LayoutInflater.from(context).inflate(R.layout.item_subdepartment, null);
        }

        ButterKnife.bind(this, convertView);

        MenuItem item = getItem(position);
        txtName.setText(item.getName());

        return convertView;
    }
}
