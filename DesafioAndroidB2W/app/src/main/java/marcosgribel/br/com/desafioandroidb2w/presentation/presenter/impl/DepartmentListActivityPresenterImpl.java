package marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl;

import android.util.Log;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.DepartmentInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.DepartmentListActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.DepartmentView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class DepartmentListActivityPresenterImpl implements DepartmentListActivityPresenter {

    private static final String TAG = DepartmentListActivityPresenterImpl.class.getSimpleName();
    private DepartmentView.List.Activity view;

    @Inject
    DepartmentInteractor interactor;


    @Inject
    public DepartmentListActivityPresenterImpl(DepartmentView view) {
        this.view = (DepartmentView.List.Activity) view;
    }


    @Override
    public void loadData(String departmentId) {
        Observable<DepartmentList> observable = interactor.getDepartmentById(departmentId);

        observable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DepartmentList>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        view.showProgress(true);
                    }

                    @Override
                    public void onCompleted() {
                        view.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, e.getMessage());
                        view.showProgress(false);
                    }

                    @Override
                    public void onNext(DepartmentList departmentList) {

                        view.fillData(departmentList);

                    }
                });
    }
}
