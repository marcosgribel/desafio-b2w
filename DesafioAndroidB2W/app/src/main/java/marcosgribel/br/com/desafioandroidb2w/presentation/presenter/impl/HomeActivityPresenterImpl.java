package marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl;

import android.util.Log;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.HomeInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Home;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.HomeActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.HomeView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class HomeActivityPresenterImpl implements HomeActivityPresenter {

    private static final String TAG = HomeActivityPresenterImpl.class.getSimpleName();

    @Inject
    HomeInteractor interactor;

    private HomeView.Activity view;


    @Inject
    public HomeActivityPresenterImpl(HomeView view) {
        this.view = (HomeView.Activity) view;
    }

    @Override
    public void carregar() {
        Observable<Home> observable = interactor.home();

        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Home>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        view.showProgress(true);
                    }

                    @Override
                    public void onCompleted() {
                        view.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showProgress(false);
                        Log.e(TAG, e.getMessage());
                    }

                    @Override
                    public void onNext(Home home) {

                        if(home.getX01() != null ){
                            view.preencherContentX01(home.getX01());
                        }

                        if(home.getX02() != null){
                            view.preencherContentX02(home.getX02());
                        }

                        if(home.getContentmiddle1() != null){
                            view.preencherContentX03(home.getContentmiddle1());
                        }

                    }
                });

    }
}

