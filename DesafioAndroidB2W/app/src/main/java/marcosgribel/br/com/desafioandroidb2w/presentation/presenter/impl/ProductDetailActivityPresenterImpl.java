package marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl;

import android.util.Log;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.ProductInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductDetailActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.ProductView;
import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class ProductDetailActivityPresenterImpl implements ProductDetailActivityPresenter {

    private static final String TAG = ProductDetailActivityPresenterImpl.class.getSimpleName();
    private ProductView.Detail.Activity view;

    @Inject
    ProductInteractor interactor;

    @Inject
    public ProductDetailActivityPresenterImpl(ProductView view) {
        this.view = (ProductView.Detail.Activity) view;
    }


    @Override
    public void loadData(String id) {

        Observable<ProductList> observable = interactor.getProductsById(id);

        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProductList>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, e.getMessage());
                    }

                    @Override
                    public void onNext(ProductList productList) {

                        if(productList != null && (productList.getProductList() != null && !productList.getProductList().isEmpty()) ){
                            view.fillData(productList.getProductList().get(0));
                        }

                    }
                });

    }
}
