package marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.ProductInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductGridActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.ProductView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class ProductGridActivityPresenterImpl implements ProductGridActivityPresenter {

    private ProductView.Grid.Activity view;

    @Inject
    ProductInteractor interactor;

    @Inject
    public ProductGridActivityPresenterImpl(ProductView view) {
        this.view = (ProductView.Grid.Activity) view;
    }

    @Override
    public void loadData(String menuId, int offset, int limit) {
        Observable<ProductList> observable = interactor.getByDepartmentFiltered(menuId, offset, limit);

        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProductList>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        view.showProgress(true);
                    }

                    @Override
                    public void onCompleted() {
                        view.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showProgress(false);
                    }

                    @Override
                    public void onNext(ProductList productList) {
                        view.fillData(productList);
                    }
                });
    }
}
