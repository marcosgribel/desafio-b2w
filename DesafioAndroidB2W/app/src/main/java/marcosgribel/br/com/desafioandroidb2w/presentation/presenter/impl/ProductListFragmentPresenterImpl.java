package marcosgribel.br.com.desafioandroidb2w.presentation.presenter.impl;

import android.util.Log;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.interactor.ProductInteractor;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductListFragmentPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.ProductView;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class ProductListFragmentPresenterImpl implements ProductListFragmentPresenter {

    private static final String TAG = ProductListFragmentPresenterImpl.class.getSimpleName();

    private ProductView.List.Fragment view;

    @Inject
    ProductInteractor interactor;


    @Inject
    public ProductListFragmentPresenterImpl(ProductView view) {
        this.view = (ProductView.List.Fragment) view;
    }


    @Override
    public void loadData() {

        Observable<DataItemList> observable = interactor.getProducts();

        observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<DataItemList>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        view.showProgress(true);
                    }

                    @Override
                    public void onCompleted() {
                        view.showProgress(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showProgress(false);
                        Log.e(TAG, e.getMessage());
                    }

                    @Override
                    public void onNext(DataItemList data) {

                        if(data != null && data.getItemListList() != null){
                            view.fillData(data.getItemListList());
                        }

                    }
                });


    }
}
