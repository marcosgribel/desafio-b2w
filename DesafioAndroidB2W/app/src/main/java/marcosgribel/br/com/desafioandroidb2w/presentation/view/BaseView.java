package marcosgribel.br.com.desafioandroidb2w.presentation.view;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface BaseView {

    void showProgress(boolean isShow);
}
