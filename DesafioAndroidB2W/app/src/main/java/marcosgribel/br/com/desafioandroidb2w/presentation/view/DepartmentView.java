package marcosgribel.br.com.desafioandroidb2w.presentation.view;

import java.util.List;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.MenuItem;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface DepartmentView extends BaseView {

    interface List {

        interface Activity extends DepartmentView {

            void fillData(DepartmentList departmentList);

        }
    }


    interface SubDepartment {

        interface List {

            interface Activity extends DepartmentView {

                void fillData(java.util.List<MenuItem> itens);

            }
        }

    }
}
