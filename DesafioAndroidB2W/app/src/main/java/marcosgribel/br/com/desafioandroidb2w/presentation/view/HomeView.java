package marcosgribel.br.com.desafioandroidb2w.presentation.view;

import marcosgribel.br.com.desafioandroidb2w.model.entity.Content;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface HomeView extends BaseView {


    interface Activity extends HomeView {

        void preencherContentX01(Content content);

        void preencherContentX02(Content content);

        void preencherContentX03(Content content);
    }
}
