package marcosgribel.br.com.desafioandroidb2w.presentation.view;


import java.util.List;

import marcosgribel.br.com.desafioandroidb2w.model.entity.ItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface ProductView extends BaseView {

    interface List {

        interface Fragment extends ProductView {

            void loadData();

            void fillData(java.util.List<ItemList> itens);


        }

    }

    interface Detail {

        interface Activity extends ProductView {

            void fillData(Product product);

        }

    }


    interface Grid {

        interface Activity extends ProductView {

            void fillData(ProductList productList);

        }

    }
}
