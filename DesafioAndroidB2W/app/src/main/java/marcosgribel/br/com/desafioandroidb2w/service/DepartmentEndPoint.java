package marcosgribel.br.com.desafioandroidb2w.service;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface DepartmentEndPoint {

    @GET("mobile_departments/{department_id}")
    Observable<DepartmentList> getDepartmentById(@Path("department_id") String departmentId);

    @GET("mobile_product_department_gallery/{department_id}")
    Observable<ProductList> getDepartmentGalleryById(@Path("department_id") String departmentId);

    @GET("mobile_products_department_offers")
    Observable<DataItemList> getDepartmentDataItem(@Query("menuId") String menuId);

}
