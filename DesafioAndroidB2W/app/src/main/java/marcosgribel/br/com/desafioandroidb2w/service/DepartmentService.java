package marcosgribel.br.com.desafioandroidb2w.service;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface DepartmentService {

    Observable<DepartmentList> getDepartmentById(String departmentId);

    Observable<ProductList> getDepartmentGalleryById(String departmentId);

    Observable<DataItemList> getDepartmentDataItem(String menuId);

}
