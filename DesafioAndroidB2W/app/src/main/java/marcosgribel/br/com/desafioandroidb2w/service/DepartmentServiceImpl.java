package marcosgribel.br.com.desafioandroidb2w.service;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Department;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import rx.Observable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class DepartmentServiceImpl implements DepartmentService {


    @Inject
    public DepartmentServiceImpl() {

    }

    @Override
    public Observable<DepartmentList> getDepartmentById(String departmentId) {
        return RetrofitConnection.connectXML(DepartmentEndPoint.class).getDepartmentById(departmentId);
    }

    @Override
    public Observable<ProductList> getDepartmentGalleryById(String departmentId) {
        return RetrofitConnection.connectXML(DepartmentEndPoint.class).getDepartmentGalleryById(departmentId);
    }

    @Override
    public Observable<DataItemList> getDepartmentDataItem(String menuId) {
        return RetrofitConnection.connectXML(DepartmentEndPoint.class).getDepartmentDataItem(menuId);
    }
}
