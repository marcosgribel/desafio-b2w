package marcosgribel.br.com.desafioandroidb2w.service;

import java.util.Date;
import java.util.Map;

import marcosgribel.br.com.desafioandroidb2w.model.entity.Data;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Home;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;
import rx.Observable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface HomeEndPoint {


    @GET("v2/spacey-api/publications/app/americanas/home_app/android")
    Observable<Home> home();



}
