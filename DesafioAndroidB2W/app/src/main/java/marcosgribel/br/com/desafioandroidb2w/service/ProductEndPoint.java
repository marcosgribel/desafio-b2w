package marcosgribel.br.com.desafioandroidb2w.service;

import android.database.Observable;

import java.util.List;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import retrofit2.http.GET;
import retrofit2.http.Query;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface ProductEndPoint {

    @GET("mobile_product_home_list")
    rx.Observable<DataItemList> getProducts();

    @GET("mobile_products_by_identifiers")
    rx.Observable<ProductList> getProductsById(@Query("productIds") String id, @Query("opn") String opn);

    @GET("mobile_products_by_department_filtered")
    rx.Observable<ProductList> getByDepartmentFiltered(@Query("menuId") String menuId,
                                                       @Query("offset") int offset,
                                                       @Query("limit") int limit);
    
}
