package marcosgribel.br.com.desafioandroidb2w.service;

import marcosgribel.br.com.desafioandroidb2w.model.entity.Data;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import retrofit2.http.Query;
import rx.Observable;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public interface ProductService {


    Observable<DataItemList> getProducts();

    Observable<ProductList> getProductsById(String id, String opn);

    rx.Observable<ProductList> getByDepartmentFiltered(String menuId, int offset, int limit);

}
