package marcosgribel.br.com.desafioandroidb2w.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import marcosgribel.br.com.desafioandroidb2w.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Copyright 2016 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 * <p>
 * Developed by BRQ IT Services
 */

public class RetrofitConnection {

    private static final OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
    private static final HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
    private static final HttpLoggingInterceptor.Level loggingLevel = HttpLoggingInterceptor.Level.BODY;



    public RetrofitConnection() {


    }


    /*

     */
    public static <S> S connectJSON(Class<S> serviceClass) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        loggingInterceptor.setLevel(loggingLevel);
        httpClientBuilder.interceptors().add(loggingInterceptor);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(BuildConfig.HTTP_API_SPACEY_AMERICANAS)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        OkHttpClient httpClient = httpClientBuilder
                .addInterceptor(loggingInterceptor)
                .build();


        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }

    /*

 */
    public static <S> S connectXML(Class<S> serviceClass) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        loggingInterceptor.setLevel(loggingLevel);
        httpClientBuilder.interceptors().add(loggingInterceptor);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(BuildConfig.HTTP_APP_AMERICANAS)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create());

        OkHttpClient httpClient = httpClientBuilder
                .addInterceptor(loggingInterceptor)
                .build();


        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }


}
