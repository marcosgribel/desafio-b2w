package marcosgribel.br.com.desafioandroidb2w.service.impl;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.model.entity.Home;
import marcosgribel.br.com.desafioandroidb2w.service.HomeEndPoint;
import marcosgribel.br.com.desafioandroidb2w.service.HomeService;
import marcosgribel.br.com.desafioandroidb2w.service.RetrofitConnection;
import rx.Observable;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class HomeServiceImpl implements HomeService {

    @Inject
    public HomeServiceImpl() {

    }

    @Override
    public Observable<Home> home() {
        return RetrofitConnection.connectJSON(HomeEndPoint.class).home();
    }


}
