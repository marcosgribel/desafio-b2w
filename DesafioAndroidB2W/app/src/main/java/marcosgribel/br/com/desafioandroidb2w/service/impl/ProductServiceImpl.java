package marcosgribel.br.com.desafioandroidb2w.service.impl;

import javax.inject.Inject;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.service.ProductEndPoint;
import marcosgribel.br.com.desafioandroidb2w.service.ProductService;
import marcosgribel.br.com.desafioandroidb2w.service.RetrofitConnection;
import rx.Observable;


/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class ProductServiceImpl implements ProductService {


    @Inject
    public ProductServiceImpl() {

    }

    @Override
    public Observable<DataItemList> getProducts() {
        return RetrofitConnection.connectXML(ProductEndPoint.class).getProducts();
    }

    @Override
    public Observable<ProductList> getProductsById(String id, String opn) {
        return RetrofitConnection.connectXML(ProductEndPoint.class).getProductsById(id, opn);
    }

    @Override
    public Observable<ProductList> getByDepartmentFiltered(String menuId, int offset, int limit) {
        return RetrofitConnection.connectXML(ProductEndPoint.class).getByDepartmentFiltered(menuId, offset, limit);
    }


}
