package marcosgribel.br.com.desafioandroidb2w.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.configuration.App;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.HomeModule;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Child;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Content;
import marcosgribel.br.com.desafioandroidb2w.presentation.adapter.BannerAdapter;
import marcosgribel.br.com.desafioandroidb2w.presentation.adapter.NavMenuAdapater;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.HomeActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.HomeView;
import marcosgribel.br.com.desafioandroidb2w.ui.fragment.ProductListFragment;

public class HomeActivity extends BaseActivity implements HomeView.Activity, NavigationView.OnNavigationItemSelectedListener {

    @Inject
    HomeActivityPresenter presenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.list_item_menu)
    ListView listViewNavMenu;

    @BindView(R.id.content_x02)
    ImageView contentX02;

    @BindView(R.id.txt_qtd_produto_oferta)
    TextView txtProdutoEmOferta;


    @BindView(R.id.recycler_view_banner)
    RecyclerView recyclerView;


    NavMenuAdapater navMenuAdapater;

    private final AdapterView.OnItemClickListener mListViewOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // ButterKnife configuration
        ButterKnife.bind(this);

        // Dagger configuration
        App.get(this)
                .getAppComponent()
                .plus(new HomeModule(this))
                .inject(this);


        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        View header = getLayoutInflater().inflate(R.layout.nav_header_main, null, false);
        listViewNavMenu.addHeaderView(header);

        navMenuAdapater = new NavMenuAdapater(this, getResources().obtainTypedArray(R.array.navigation_list_icon), getResources().getStringArray(R.array.navigation_list));
        listViewNavMenu.setAdapter(navMenuAdapater);
        listViewNavMenu.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        listViewNavMenu.setOnItemClickListener(mListViewOnItemClickListener);
        selectItem(1);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);


    }

    @Override
    protected void onResume() {
        super.onResume();

        load();

    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void preencherContentX01(Content content) {
        BannerAdapter adapter = new BannerAdapter(this);
        recyclerView.setAdapter(adapter);

        List<Child> children = content.getComponent().getChildren();
        adapter.addAll(children);

        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void preencherContentX02(Content content) {
        List<Child> children = content.getComponent().getChildren();
        Child child = children.get(0);

        Glide.with(this)
                .load(child.getImage())
                .into(contentX02);
        contentX02.setVisibility(View.VISIBLE);
    }

    @Override
    public void preencherContentX03(Content content) {
        int count = content.getComponent().getChildren().size();
        txtProdutoEmOferta.setText( String.format( getString(R.string.texto_quantidade_produtos), String.valueOf(count) ));
    }

    private void load(){

        presenter.carregar();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, new ProductListFragment())
                .commit();

    }

    private void selectItem(int position) {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }

        this.listViewNavMenu.setItemChecked(position, true);
        switch (position){
            case 1:
                break;
            case 2:
                startActivity(DepartmentListActivity.newActivity(this, getString(R.string.departamentos)));
                break;
        }

    }
}
