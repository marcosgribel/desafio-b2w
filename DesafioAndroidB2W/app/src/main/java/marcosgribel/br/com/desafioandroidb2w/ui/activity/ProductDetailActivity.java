package marcosgribel.br.com.desafioandroidb2w.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.Serializable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.configuration.App;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.ProductModule;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;
import marcosgribel.br.com.desafioandroidb2w.presentation.adapter.HorizontalProductThumbAdapter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductDetailActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.ProductView;

public class ProductDetailActivity extends BaseActivity implements ProductView.Detail.Activity {

    @Inject
    ProductDetailActivityPresenter presenter;

    @BindView(R.id.recyclerview_thumb_gallery)
    RecyclerView recyclerViewThumbGallery;

    @BindView(R.id.txt_product_name)
    TextView txtName;
    @BindView(R.id.txt_product_id)
    TextView txtId;
    @BindView(R.id.txt_product_reviews)
    TextView txtReviews;
    @BindView(R.id.txt_product_price)
    TextView txtPrice;



    public static Intent newActivity(Context context, Product product){
        Intent intent = new Intent(context, ProductDetailActivity.class);
        intent.putExtra("product", (Serializable) product);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        // Dagger configuration
        App.get(this)
                .getAppComponent()
                .plus(new ProductModule(this))
                .inject(this);


        Bundle args = getIntent().getExtras();
        Product product = (Product) args.getSerializable("product");
        presenter.loadData(product.getProdId());

    }



    @Override
    public void fillData(Product product){
        if (product == null ){
            return;
        }

        HorizontalProductThumbAdapter thumbAdapter = new HorizontalProductThumbAdapter(this, product.getImageList());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewThumbGallery.setLayoutManager(layoutManager);
        recyclerViewThumbGallery.setHasFixedSize(true);
        recyclerViewThumbGallery.setNestedScrollingEnabled(false);
        recyclerViewThumbGallery.setAdapter(thumbAdapter);

        txtName.setText(product.getName());
        txtId.setText( String.format(getString(R.string.txt_codigo_do_produto), product.getProdId()) );
        txtReviews.setText( String.format(getString(R.string.txt_num_avaliacoes), product.getNumReviews().toString()) );
        txtPrice.setText(product.getPrice());
    }
}
