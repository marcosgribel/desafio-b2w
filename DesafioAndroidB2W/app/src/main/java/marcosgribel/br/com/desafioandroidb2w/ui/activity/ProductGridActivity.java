package marcosgribel.br.com.desafioandroidb2w.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.configuration.App;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.ProductModule;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.presentation.adapter.GridProductAdapter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductGridActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.ProductView;

public class ProductGridActivity extends BaseActivity implements ProductView.Grid.Activity, AbsListView.OnScrollListener {

    @Inject
    ProductGridActivityPresenter presenter;


    @BindView(R.id.gridview_product)
    GridView gridView;
    private GridProductAdapter adapter;

    private String mMenuId;
    private String mTitle;
    private int mLimit = 20;
    private int mOffset = 0;
    private int mCurrentPage = 0;

    private final AdapterView.OnItemClickListener mGridViewOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Product product = adapter.getItem(position);
            startActivity(ProductDetailActivity.newActivity(ProductGridActivity.this, product));
        }
    };


    public static Intent newActivity(Context context, String menuId, String subDeptName){
        Intent intent = new Intent(context, ProductGridActivity.class);
        intent.putExtra("menu_id", menuId);
        intent.putExtra("sub_department_name", subDeptName);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_grid);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        App.get(this)
                .getAppComponent()
                .plus(new ProductModule(this))
                .inject(this);


        adapter = new GridProductAdapter(this);
        gridView.setAdapter(adapter);
        gridView.setOnScrollListener(this);
        gridView.setOnItemClickListener(mGridViewOnItemClickListener);

        if(savedInstanceState == null ){

            Bundle args = getIntent().getExtras();
            this.mMenuId = args.getString("menu_id");
            this.mTitle = args.getString("sub_department_name");

            loadData();

        } else {

            this.mMenuId = savedInstanceState.getString("menu_id");
            this.mTitle = savedInstanceState.getString("sub_department_name");
            this.mCurrentPage = savedInstanceState.getInt("current_page");
            this.mOffset = savedInstanceState.getInt("offset");
            this.mLimit = savedInstanceState.getInt("limit");

            ArrayList<Product> itens = savedInstanceState.getParcelableArrayList("itens");
            adapter.addAll(itens);
        }

        getSupportActionBar().setTitle(mTitle);


    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("menu_id", this.mMenuId);
        outState.putString("sub_department_name", this.mTitle);
        outState.putInt("current_page", this.mCurrentPage);
        outState.putInt("offset", this.mOffset);
        outState.putInt("limit", this.mLimit);
        outState.putParcelableArrayList("itens", (ArrayList<? extends Parcelable>) adapter.getItens());
    }

    public void loadData(){

        presenter.loadData(this.mMenuId, this.mOffset, this.mLimit);

    }

    public boolean hasMoreItens(){
        if(adapter.getCount() == 0){
            return true;
        }

        if( (this.mCurrentPage * this.mLimit) == adapter.getCount()){
            return true;
        }

        return false;
    }


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        Log.d("", "onScrollStateChanged");
        if(hasMoreItens()){
            loadData();
        }
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        Log.d("", "onScroll");
    }

    @Override
    public void fillData(ProductList productList) {
            if(adapter == null){
                return;
            }

        this.adapter.addAll(productList.getProductList());
        this.mCurrentPage ++;
        this.mOffset += 20;
    }
}
