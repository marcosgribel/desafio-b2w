package marcosgribel.br.com.desafioandroidb2w.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.configuration.App;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.DepartmentModule;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Department;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.presentation.adapter.SubDepartmentAdapter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.DepartmentListActivityPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.DepartmentView;

public class SubDepartmentListActivity extends BaseActivity implements DepartmentView.List.Activity {


    @Inject
    DepartmentListActivityPresenter presenter;


    @BindView(R.id.txt_title_department)
    TextView txtDepartmentName;
    @BindView(R.id.listview_subdepartments)
    ListView listView;


    SubDepartmentAdapter adapter;

    private String title;
    private String menuId;

    public static Intent newActivity(Context context, String menuId, String subDeptName){
        Intent intent = new Intent(context, SubDepartmentListActivity.class);
        intent.putExtra("menu_id", menuId);
        intent.putExtra("sub_department_name", subDeptName);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_department);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        App.get(this)
                .getAppComponent()
                .plus(new DepartmentModule(this))
                .inject(this);


        if( savedInstanceState == null ){
            Bundle args = getIntent().getExtras();
            title = args.getString("sub_department_name");
            menuId = args.getString("menu_id");
        } else {
            title = savedInstanceState.getString("sub_department_name");
            menuId = savedInstanceState.getString("menu_id");
        }

        getSupportActionBar().setTitle(title);


        adapter = new SubDepartmentAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(mListViewOnItemClickListener);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedInstanceState.putString("sub_departmensavedInstanceState.putString(\"sub_department_name\", title);t_name", title);
        savedInstanceState.putString("menu_id", menuId);
    }


    @Override
    protected void onResume() {
        super.onResume();
        load();
    }


    private void load(){
        presenter.loadData(menuId);

    }

    @Override
    public void fillData(DepartmentList departmentList) {
        if( departmentList == null ){
            return;
        }

        txtDepartmentName.setText(title);

        Department department = departmentList.getDepartmentList().get(0);
        adapter.addAll(department.getMenuItemList());
    }


    private final AdapterView.OnItemClickListener mListViewOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            marcosgribel.br.com.desafioandroidb2w.model.entity.MenuItem item =  adapter.getItem(position);
            startActivity(ProductGridActivity.newActivity(SubDepartmentListActivity.this, item.getMenuId(), item.getName()));
        }
    };
}
