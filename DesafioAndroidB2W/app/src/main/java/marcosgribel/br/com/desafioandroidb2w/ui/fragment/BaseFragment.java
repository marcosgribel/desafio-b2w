package marcosgribel.br.com.desafioandroidb2w.ui.fragment;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.BindView;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.BaseView;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class BaseFragment extends Fragment implements BaseView {

    @Nullable
    @BindView(R.id.progress)
    ProgressBar progress;

    public void showProgress(boolean isShow) {
        if( progress == null ){
            return;
        }

        if(isShow){
            progress.setVisibility(View.VISIBLE);
        }else{
            progress.setVisibility(View.GONE);
        }
    }
}
