package marcosgribel.br.com.desafioandroidb2w.ui.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import marcosgribel.br.com.desafioandroidb2w.R;
import marcosgribel.br.com.desafioandroidb2w.configuration.App;
import marcosgribel.br.com.desafioandroidb2w.configuration.di.module.ProductModule;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ItemList;
import marcosgribel.br.com.desafioandroidb2w.presentation.adapter.ItemListAdapter;
import marcosgribel.br.com.desafioandroidb2w.presentation.presenter.ProductListFragmentPresenter;
import marcosgribel.br.com.desafioandroidb2w.presentation.view.ProductView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductListFragment extends BaseFragment implements ProductView.List.Fragment {

    @Inject
    ProductListFragmentPresenter presenter;

    @BindView(R.id.product_list_view)
    ListView listView;

    private ItemListAdapter adapter;

    public ProductListFragment() {
        // Required empty public constructor
    }


    public static ProductListFragment newInstance() {
        Bundle args = new Bundle();
        
        ProductListFragment fragment = new ProductListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //  Butterknife configuration
        ButterKnife.bind(this, view);

        //  Dagger configuration
        App.get(getContext())
                .getAppComponent()
                .plus(new ProductModule(this))
                .inject(this);


        adapter = new ItemListAdapter(getContext());
        listView.setAdapter(adapter);
        listView.setNestedScrollingEnabled(false);

    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void loadData() {
        presenter.loadData();
    }

    @Override
    public void fillData(java.util.List<ItemList> itens) {
        adapter.addAll(itens);

        setListViewHeightBasedOnChildren(listView);
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        if(listView == null){
            return;
        }
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }
}
