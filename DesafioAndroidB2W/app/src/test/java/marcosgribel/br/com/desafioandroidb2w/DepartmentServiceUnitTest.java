package marcosgribel.br.com.desafioandroidb2w;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Department;
import marcosgribel.br.com.desafioandroidb2w.model.entity.DepartmentList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.service.DepartmentService;
import marcosgribel.br.com.desafioandroidb2w.service.DepartmentServiceImpl;
import marcosgribel.br.com.desafioandroidb2w.service.impl.ProductServiceImpl;
import rx.Observable;
import rx.observers.TestSubscriber;
import rx.schedulers.TestScheduler;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class DepartmentServiceUnitTest  {

    DepartmentService service;


    @Before
    public void setUp(){
        service = new DepartmentServiceImpl();
    }



    @Test
    public void getDepartmentByIdTest() {

        String departmentId = "226708";

        TestSubscriber<DepartmentList> subscriber = new TestSubscriber<>();
        Observable<DepartmentList> observable = service.getDepartmentById(departmentId);

        observable.subscribe(subscriber);
        subscriber.assertNoErrors();
        subscriber.assertCompleted();

        List<DepartmentList> list = subscriber.getOnNextEvents();

        for (DepartmentList d :list) {
            Assert.assertNotNull(d.getDepartmentList());
        }
    }

    @Test
    public void getDepartmentGalleryByIdTest() {

    }

    @Test
    public void getDepartmentDataItemTest() {

    }
}
