package marcosgribel.br.com.desafioandroidb2w;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/

import org.junit.Before;
import org.junit.Test;

import marcosgribel.br.com.desafioandroidb2w.model.entity.Home;
import marcosgribel.br.com.desafioandroidb2w.service.HomeService;
import marcosgribel.br.com.desafioandroidb2w.service.impl.HomeServiceImpl;
import rx.Observable;
import rx.observers.TestSubscriber;

public class HomeServiceTest {

    HomeService service;

    @Before
    public void setUp(){
        service = new HomeServiceImpl();
    }


    @Test
    public void home(){
        TestSubscriber<Home> subscriber = new TestSubscriber<>();
        Observable<Home> observable = service.home();

        observable.subscribe(subscriber);
        subscriber.assertNoErrors();
        subscriber.onCompleted();

    }


}
