package marcosgribel.br.com.desafioandroidb2w;

import com.google.gson.Gson;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import marcosgribel.br.com.desafioandroidb2w.model.entity.DataItemList;
import marcosgribel.br.com.desafioandroidb2w.model.entity.Product;
import marcosgribel.br.com.desafioandroidb2w.model.entity.ProductList;
import marcosgribel.br.com.desafioandroidb2w.service.ProductService;
import marcosgribel.br.com.desafioandroidb2w.service.impl.ProductServiceImpl;
import rx.Observable;
import rx.observers.TestSubscriber;

/*
* Copyright (c) 2017 - All Rights Reserved
* Unauthorized copying of this file, via any medium is strictly prohibited.
* Proprietary and confidential.
*
* @author Marcos Gribel - gribel.marcos@gmail.com
*/
public class ProductServiceUniTest {

    ProductService service;

    @Before
    public void setUp(){
        service = new ProductServiceImpl();
    }


    @Test
    public void getProductsTest(){

        long ids[] = {
                124132646, 128361278
        };

        TestSubscriber<DataItemList> subscriber = new TestSubscriber<>();
        Observable<DataItemList> observable = service.getProducts();

        observable.subscribe(subscriber);
        subscriber.assertNoErrors();
        subscriber.onCompleted();

        List<DataItemList> events = subscriber.getOnNextEvents();
        for (DataItemList d : events) {
            String json = new Gson().toJson(d);
            System.err.print(json);
        }
    }


    @Test
    public void getProductsByIdTest(){

        String id = "127531749";

        TestSubscriber<ProductList> subscriber = new TestSubscriber<>();
        Observable<ProductList> observable = service.getProductsById(id, null);

        observable.subscribe(subscriber);
        subscriber.assertNoErrors();
        subscriber.onCompleted();

        List<ProductList> events = subscriber.getOnNextEvents();
        for (ProductList d : events) {
            for (Product p : d.getProductList()) {
                String json = new Gson().toJson(p);
                System.err.print(json);
            }

        }
    }


    @Test
    public void getByDepartmentFiltered(){
        String menuId = "443653";
        int offset = 0;
        int limit = 10;

        TestSubscriber<ProductList> subscriber = new TestSubscriber<>();
        Observable<ProductList> observable = service.getByDepartmentFiltered(menuId, offset, limit);

        observable.subscribe(subscriber);
        subscriber.assertNoErrors();
        subscriber.onCompleted();

        List<ProductList> events = subscriber.getOnNextEvents();
        for (ProductList d : events) {
            Assert.assertNotNull(d.getProductList());
            Assert.assertEquals(d.getProductList().size(), limit);

            for (Product p : d.getProductList()) {
                String json = new Gson().toJson(p);
                System.err.print(json);
            }

        }

    }
}
